import time

def retry(*exceptions, attempts=3, initial_delay=3, backoff=2):
    def decorator(func):
        def wrapper(*args, **kwargs):
            delay = initial_delay
            for _ in range(attempts):
                try:
                    result = func(*args, **kwargs)
                    return result
                except exceptions as e:
                    print(f"Caught exception {e}, retrying in {delay} seconds...")
                    time.sleep(delay)
                    delay *= backoff
            raise Exception(f"Function {func.__name__} failed after {attempts} attempts")

        return wrapper
    return decorator

@retry(ValueError, StopIteration, attempts=3, initial_delay=3, backoff=2)
def tests(i=2):
    if i % 2 == 0:
        raise ValueError
    return i

@retry(SystemExit, attempts=10, initial_delay=0.5, backoff=1)
def print_logs():
    pass

if __name__ == "__main__":
    try:
        # Вызываем функцию tests
        result = tests(i=2)
        print(f"Result of tests: {result}")
    except Exception as e:
        print(f"Error in tests: {e}")

    try:
        # Вызываем функцию print_logs
        print_logs()
    except SystemExit:
        print("print_logs завершил выполнение с SystemExit")
    except Exception as e:
        print(f"Произошла ошибка: {e}")
